import datetime

class Validator:
    
    def __init__(self,name):
        self.name = name

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return getattr(instance, self.name)

    def __set__(self, instance, value):
        if not isinstance(value, str):
            raise ValueError("Value must be a string")
        setattr(instance, self.name, value)

    def __delete__(self, instance):
        delattr(instance, self.name)

class Converter:
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        return getattr(instance, self.name)

    def __set__(self, instance, value):
        if not isinstance(value, str):
            raise ValueError("Value must be a string")
        setattr(instance, self.name, value.upper())
    
    def __call__(self, value):
        if not isinstance(value, str):
            raise ValueError("Value must be a string")
        return value.upper()
    
class Audit_Log:
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        return getattr(instance, self.name)

    def __set__(self, instance, value):
        previous_value = getattr(instance, self.name, None)
        setattr(instance, self.name, value)
        timestamp = datetime.datetime.now()
        if not hasattr(instance, '_audit_log'):
            instance._audit_log = []
        instance._audit_log.append((timestamp, self.name, previous_value, value))

    def __delete__(self, instance):
        delattr(instance, self.name)

class NumericRange:
    def __init__(self, name, min_value=None, max_value=None, audit_logger=None):
        self._name = name
        self.min_value = min_value
        self.max_value = max_value
        self._value = None  
        self.audit_logger=audit_logger

    def __get__(self, instance, owner):
        return self._value

    def __set__(self, instance, value):
        if not isinstance(value, (int, float)):
            raise ValueError("Value must be a numeric type")
        if self.min_value is not None and value < self.min_value:
            raise ValueError(f"Value must be greater than or equal to {self.min_value}")
        self._value = value  

    def __delete__(self, instance):
        delattr(instance, self._name)

class StringFormat:
    def __init__(self, name, prefix=None, suffix=None, validator=None, audit_logger=None, converter=None):
        self._name = name
        self.prefix = prefix
        self.suffix = suffix
        self._value = None
        self.validator = validator
        self.converter = converter
        self.audit_logger = audit_logger

    def __get__(self, instance, owner):
        return self._value

    def __set__(self, instance, value):
        
        if self.prefix is not None and not value.startswith(self.prefix):
            raise ValueError(f"Value must start with '{self.prefix}'")
        if self.suffix is not None and not value.endswith(self.suffix):
            raise ValueError(f"Value must end with '{self.suffix}'")
        self._value = self.converter(value)

    def __delete__(self, instance):
        delattr(instance, self._name)

class MyClass:
    age = NumericRange("age", min_value=0, max_value=100,audit_logger=Audit_Log('age'))
    name = StringFormat("name", prefix="Ms.", validator=Validator('name'),converter=Converter('name'),audit_logger=Audit_Log('name'))

    def __init__(self, age, name):
        self.age = age
        self.name = name

obj = MyClass(25, "Ms. Sandra")
print(obj.age)  
print(obj.name)  
obj.age = 45 
#obj.name = "Anna"  #raise ValueError, "Value must start with 'Ms."

print(obj.age)  
print(obj.name)  
