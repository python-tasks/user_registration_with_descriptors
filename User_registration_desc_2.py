import re
import hashlib
from datetime import datetime


class Descriptor:
    def __init__(self):
        self.value = {}
    def __get__(self, instance, owner):
        if instance is None:
            return self
        return self.value.get(instance)    
        
class String_Descriptor(Descriptor):
    def __init__(self):
        return super().__init__()

    def __set__(self, instance, value):
        if not isinstance(value, str):
            raise ValueError("value must be a string")
        self.value[instance] = value

class Email_Descriptor(Descriptor):
    def __init__(self):
        super().__init__()

    def __set__(self, instance, value):
        pattern = r'^[\w\.-]+@[\w\.-]+\.\w+$'
        if not re.match(pattern, value):
            raise ValueError("Invalid email format")
        self.value[instance] = value

class Password_Descriptor:
    def __init__(self, min_length=8, require_digit=True, require_upper=True, require_lower=True):
        self.min_length = min_length
        self.require_digit = require_digit
        self.require_upper = require_upper
        self.require_lower = require_lower
        self._password_hash = None

    def __get__(self, instance, owner):
        if instance is None:
            return self
        return self._password_hash

    def __set__(self, instance, value):
        self._validate_and_hash_password(value)

    def _validate_and_hash_password(self, password):
        self._validate_password(password)
        self._password_hash = self._hash_password(password)

    def _validate_password(self, password):
        if len(password) < self.min_length:
            raise ValueError(f"Password must be at least {self.min_length} characters long")

        if self.require_digit and not any(char.isdigit() for char in password):
            raise ValueError("Password must contain at least one digit")

        if self.require_upper and not any(char.isupper() for char in password):
            raise ValueError("Password must contain at least one uppercase letter")

        if self.require_lower and not any(char.islower() for char in password):
            raise ValueError("Password must contain at least one lowercase letter")

    def _hash_password(self, password):
        return hashlib.sha256(password.encode()).hexdigest()

class BDate_Descriptor(Descriptor):
    def __init__(self):
        super().__init__()

    def __set__(self, instance, value):
        if not self._validate_age(value):
            raise ValueError("Age must be 18 or older")
        self.value[instance] = value

    def _validate_age(self, birth_date):
        today = datetime.now()
        age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
        return age >= 18
        
class User:
    first_name = String_Descriptor()
    last_name = String_Descriptor()
    birthDate = BDate_Descriptor()
    email = Email_Descriptor()
    password = Password_Descriptor()

user1 = User()
user1.first_name = "Sandra"
user1.last_name = "Rivera"
user1.birthDate = datetime.strptime('1967-12-06', '%Y-%m-%d')
user1.email = "example@example.com"
user1.password = "Sandra1234"
print(f"User Registration Details: \nFirst name: {user1.first_name} \nLast name: {user1.last_name} \nBirth date: {user1.birthDate.strftime('%Y-%m-%d')} \nEmail: {user1.email} \nHashed password: {user1.password}")
